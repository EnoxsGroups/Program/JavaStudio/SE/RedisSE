package com.enoxs.domain.app.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DataServiceTest {

    @Autowired
    private DataService dataService;

    @Test
    void createData() {
        dataService.createData("a" , "001");
    }

    @Test
    void queryData() {
        String key = "a";
        String msg = dataService.queryData(key);
        System.out.println(msg);
    }
}