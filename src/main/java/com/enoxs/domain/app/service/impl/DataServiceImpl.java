package com.enoxs.domain.app.service.impl;

import com.enoxs.domain.app.service.DataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class DataServiceImpl implements DataService {
    private final static Logger logger = LoggerFactory.getLogger(DataServiceImpl.class);

    private final Long TIME_OUT = 1800L; // 缓存超时时间

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void createData(String key, String val) {
        ValueOperations<String, String> ops = this.stringRedisTemplate.opsForValue();
        // 先查缓存，没有再查服务
        if (!this.stringRedisTemplate.hasKey(key)) {
            logger.info("不存在 key " + key);
            ops.set(key, val, TIME_OUT, TimeUnit.SECONDS);
        } else {
            logger.info("存在 key " + key + ", value=" + ops.get(key));

        }
    }

    @Override
    public String queryData(String key) {
        return this.stringRedisTemplate.opsForValue().get(key);
    }
}
