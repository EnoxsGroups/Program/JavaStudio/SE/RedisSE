package com.enoxs.domain.app.service;

public interface DataService {

    void createData(String key, String val);

    String queryData(String key);

}
